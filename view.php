<form method="POST" action="<?php echo $this->action; ?>" class="no-js">

    <div class="row">

        <div class="col-12 col-md-6">
            <label for="firstname">Firstname</label>
            <input type="text" class="form-control" name="firstname" />
        </div>

        <div class="col-12 col-md-6">
            <label for="lastname">Lastname</label>
            <input type="text" class="form-control" name="lastname" />
        </div>

    </div>

    <input type="hidden" name="submitForm" value="true" />

    <noscript>
        <input type="submit" class="btn btn-primary" value="Submit" />
    </noscript>

    <button id="submit-form" class="btn btn-primary" onclick="submitForm('<?php echo $this->action; ?>');">Submit</button>

</form>