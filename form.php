<?php
/**
 * Contact Form ACF Module
 */

/**
 * Todo: Find a way to dynamically get current file
 */

include_once get_stylesheet_directory() . '/form/controller.php';


function ajax_form_handler() {
    echo json_encode( $form->submit($_POST) );
    exit();
}


$form = new \ContactForm\FormController();
$form->setAction(get_stylesheet_directory_uri() . '/template-parts/modules/form.php');

$form->setRequiredFields(
    array('firstname')
);

if (isset($_POST['submitForm']) && $_POST['submitForm'] == 'true')
    $form->submit($_POST);
else 
    $form->display();