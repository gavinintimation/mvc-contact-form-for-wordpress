<?php

namespace ContactForm;

interface ContactForm
{
    public function display();
    public function setRequiredFields();
    public function submit();
}

class FormController implements ContactForm
{
    private $action;
    private $requiredFields;

    public function setAction($action) {
        $this->action = $action;
    }

    public function setRequiredFields($fields) {
        $this->requiredFields = $fields;
    }

    public function display() {
        include('view.php');
    }

    private function checkRequiredFields($data, &$errors) {
        foreach ($this->requiredFields as $rf) {
            if ( empty($data[$rf]) || $data[$rf] === 'false' ) {
                $errors[] = array('field' => $rf, 'error' => $rf . ' is required.');
            }
        }
    }

    public function submit($data) {

        $result = array();
        $errors = array();

        if ( !empty($this->requiredFields) ) {
            $this->checkRequiredFields($data, $errors);
        }

        /**
         * Check if email address is valid
         */

        if ( !empty($data['email']) && !filter_var($data['email'], FILTER_VALIDATE_EMAIL) ) {
            $errors[] = array('field' => 'email', 'error' => 'Invalid email address entered.');
        }

        if (!empty($errors)) {
            $result['errors'] = $errors;
            return $result;
        }

        $name = filter_var($data['firstname'] . ' ' . $data['lastname'], FILTER_SANITIZE_STRING);
        $email = filter_var($data['email'], FILTER_SANITIZE_EMAIL);

    }
}