const submitForm = (action) => {

	let req = new XMLHttpRequest();
	
	const data = {
		firstname: document.querySelector('input[name="firstname"]')[0].value,
		lastname: document.querySelector('input[name="lastname"]')[0].value,
		email: document.querySelector('input[name="email"]')[0].value,
		phone: document.querySelector('input[name="phone"]')[0].value,
		subject: document.querySelector('input[name="subject"]')[0].value,
		message: document.querySelector('textarea[name="message"]')[0].value
	};
	
	if (!req) return false;
    if (typeof success != 'function') success = () => {};
    if (typeof error != 'function') error = () => {};
	
    req.onreadystatechange = () => {
        if (req.readyState == 4) {
            return req.status === 200 ? 
                success(req.responseText) : error(req.status)
            ;
        }
    };
	
	req.open("POST", action, true);
    req.send(data);
    return req;

};